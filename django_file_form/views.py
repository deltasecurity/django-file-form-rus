import json
from django.core.exceptions import PermissionDenied
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, HttpResponseNotFound
from django.views import generic

from .models import UploadedFile
from .uploader import FileFormUploader
from . import conf

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator



handle_upload = csrf_exempt(FileFormUploader())


class DeleteFile(generic.View):
    def post(self, request, file_id=None):
        file_id = file_id or request.POST.get('qquuid')

        return self.delete_file(request, file_id)

    def delete(self, request, file_id):
        return self.delete_file(request, file_id)

    def delete_file(self, request, file_id):
        if conf.MUST_LOGIN and not request.user.is_authenticated():
            raise PermissionDenied()

        uploaded_file = UploadedFile.objects.try_get(file_id=file_id)

        if uploaded_file:
            uploaded_file.delete()
            return HttpResponse(
                json.dumps({'result': 'ok', 'qquuid': file_id}, cls=DjangoJSONEncoder),
                content_type='application/json; charset=utf-8'
            )
        else:
            return HttpResponseNotFound()

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DeleteFile, self).dispatch(request, *args, **kwargs)
